import time


def log(*args, **kwargs):
    with open('log.txt', 'a', encoding='utf-8') as f:
        print(time.strftime('%Y/%m/%d %H:%M:%S'), *args, file=f, **kwargs)


def random_letters(lenth):
    import random
    import string
    return ''.join(random.sample(string.ascii_letters, lenth))
