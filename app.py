from flask import Flask

from config import secret_key
from route.blog import main as blog_route
from route.index import main as index_route
from route.toys import main as toys_route
from route.about import main as about_route
from route.user import main as user_route
from route.bbs import main as bbs_route


def configured_app():
    app = Flask(__name__)
    app.secret_key = secret_key
    registers(app)
    return app


def registers(app):
    app.register_blueprint(index_route, url_prefix='/')
    app.register_blueprint(blog_route, url_prefix='/blog')
    app.register_blueprint(toys_route, url_prefix='/toys')
    app.register_blueprint(about_route, url_prefix='/about')
    app.register_blueprint(user_route, url_prefix='/user')
    app.register_blueprint(bbs_route, url_prefix='/bbs')


if __name__ == '__main__':
    app = configured_app()
    config = dict(
        debug=True,
        host='0.0.0.0',
        port=2000,
    )
    app.run(**config)
