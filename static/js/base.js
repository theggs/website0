/**
 * Created by Fanwu on 2017/6/12.
 */
// 基本函数
var log = function () {
    console.log.apply(console, arguments)
}

var e = function (args) {
    return document.querySelector(args)
}

var eall = function (args) {
    return document.querySelectorAll(args)
}

//AJAX
var ajax = function(method, path, data, responseCallBack){
    var r = new XMLHttpRequest()
    r.open(method, path, true)
    r.setRequestHeader('Content-Type', 'application/json')
    r.onreadystatechange = function () {
        if(r.readyState === 4) {
            responseCallBack(r.response)
        }
    }
    data = JSON.stringify(data)
    r.send(data)
}

// 时间转换
var transTimePast = function(time_now, time) {
    var timeReturn = '某个神秘时间'
    var timePast = time_now / 1000 - time
    if(timePast <= 60*3) {
        timeReturn = `${parseInt(timePast)}s ago`
    }
    else if(timePast <= 60*60) {
        timeReturn =  `${parseInt(timePast/60)}m ago`
    }
    else if(timePast <= 60*60*24) {
        timeReturn = `${parseInt(timePast/60/60)}h ago`
    }
    else if(timePast <= 60*60*24*3) {
        timeReturn = `${parseInt(timePast/60/60/24)}d ago`
    }
    else {
        timeReturn = `on ${new Date(time*1000).toDateString()}`
    }
    return timeReturn
}
var transTime_static = function () {
    var timePostDivs = eall('.trans_time')
    var i = timePostDivs.length
    var time_now = new Date().getTime()
    while(i--) {
        var timePost = Number(timePostDivs[i].dataset.time)
        timePost = transTimePast(time_now, timePost)
        timePostDivs[i].innerText = timePost
    }
}
var transTime = function () {
    setInterval(function () {
        transTime_static()
    }, 1000)
}

// 获取 Url query
var getUrlVars = function ()
{
    var vars = [], hash;
    var url = decodeURI(window.location.href)
    var hashes = url.slice(url.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
