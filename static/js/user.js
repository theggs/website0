/**
 * Created by Fanwu on 2017/6/24.
 */
var apiGetCurrentUserProfile = function (callback) {
    var path = '/user/profile'
    ajax('GET', path, '', callback)
}

var insertCurrentUserProfile = function () {
    apiGetCurrentUserProfile(function (r) {
        e('#user_profile').insertAdjacentHTML('afterBegin', r)

        var showUploadAvatorForm = function () {
            $('#btn_update_avator').addClass('hidden')
            $('#form_update_avator').removeClass('hidden')
        }
        if (e('#btn_update_avator') !== null) {
            e('#btn_update_avator').addEventListener('click', function () {
            showUploadAvatorForm()
            })
        }

    })
}