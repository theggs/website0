/**
 * Created by Fanwu on 2017/6/13.
 */
// API
var apiShortURL = function (form, callback) {
    var path = '/toys/shorturl'
    ajax('POST', path, form, callback)
}


// 各项
// --shorturl
var clean_result = function() {  //清除已显示的结果
    var s = e('#result')
    if(s !== null){
        s.parentNode.removeChild(s)
    }
}
var clickShortURL = function(form, position) {
    clean_result()
    if(position.value !== '') {
        position.value = ''
        apiShortURL(form, function(r) {
            var resultURL = JSON.parse(r)
            e('#shortURL_result').value = resultURL
        })
    }
}
var bindEventShortURL = function () {
    var button1 = e('#button_OriginalURL')
    button1.addEventListener('click', function() {
        var input1 = e('#input_OriginalURL')
        var form = {
        new: input1.value
        }
        clickShortURL(form, input1)
    })
    var button2 = e('#button_ShortURL')
    button2.addEventListener('click', function() {
        var input2 = e('#input_ShortURL')
        var form = {
        lookup: input2.value
        }
        clickShortURL(form, input2)
    })
}
