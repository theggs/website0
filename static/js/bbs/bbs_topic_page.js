/**
 * Created by the-e on 2017/7/4.
 */

var md = new markdownit()

// {# markdown 文本内容 #}
var markdownArticle = function () {
    var md = new markdownit()
    var contents = eall('.md-content')
    var i = contents.length
    while (i--) {
        var contentText = contents[i].textContent
        contents[i].innerHTML = md.render(contentText)
    }
}

// 获取 comments
//   插入回复区框架
var insert_comments_panel = function () {
    var contentHtml = `
            <!-- 回复区 -->
            <div class="panel panel-default" id="comments">
                <div class="panel-body" id="comments-body">
                </div>
            </div>`
    e('#topic').insertAdjacentHTML('afterEnd', contentHtml)
}
//   插入回复
var insert_comments = function (result, page) {
    var i = result.length
    while(i--) {
        var floor = (page - 1) * 10 + i + 1
        var comment = result[i]
        var contentHtml = `
            <div>
                <span>${floor}楼</span>
                <a href="user/${comment.user_id}"><strong>${comment.user_username}</strong></a>
                <small class="trans_time" data-time="${comment.ct}"></small>
                <div>${md.render(comment.content)}</div>
                <hr>
            </div>`
        e('#comments-body').insertAdjacentHTML('afterBegin', contentHtml)
    }
    if ($('.page').hasClass('disabled')) {
        $('.page').removeClass('disabled')
    }
    $(`#page${page}`).addClass('disabled')
}
//   插入分页
var insert_comments_pagination = function () {
    var num_of_comments = e('#num_of_comments').innerText
    num_of_comments = parseInt(num_of_comments)
    var num_of_pages = Math.ceil(num_of_comments / 10)
    var pageination_frame = `
            <nav aria-label="Page navigation" class="container-fluid">
              <ul class="pagination">
              </ul>
            </nav>`
    e('#comments').insertAdjacentHTML('beforeEnd', pageination_frame)
    while(num_of_pages) {
        var pageination_content = `<li class="page" id="page${num_of_pages}"><a class="page_a">${num_of_pages}</apage_a></li>`
        e('.pagination').insertAdjacentHTML('afterBegin', pageination_content)
        num_of_pages--
    }
}
//   绑定分页按钮事件
var clickGetComments_Page = function(form) {
    apiComments(form, function(r) {
        var result = JSON.parse(r)
        e('#comments-body').innerHTML = ''
        insert_comments(result, form.comments_page)
    })
}

var bindEventGetComments_Page = function() {
    var btns = e('.pagination')
    btns.addEventListener('click', function (event) {
        var btn = event.target
        if(btn.classList.contains('page_a')) {
            var form = {
                'topic_id': parseInt(e('#topic_id').value),
                'comments_page': parseInt(btn.text),
            }
            clickGetComments_Page(form)
            $("html,body").animate({ scrollTop: 0}, 200)
        }
    })
}

//   载入页面时加载
var getCommentsOnLoad = function() {
    var page = 1
    var form = {
        'topic_id': parseInt(e('#topic_id').value),
        'comments_page': page,
        }
    apiComments(form, function(r) {
        var result = JSON.parse(r)
        if(result !== null) {
            insert_comments_panel()
            insert_comments_pagination()
            insert_comments(result, page)
            bindEventGetComments_Page()
        }
    })
}



// 提交新的 comment
var insert_new_comment = function (result) {
    if (e('#comments-body') === null) {
        insert_comments_panel()
    }
    var contentHtml = `
        <div>
            <a href="/user/${result.author_id}"><strong>${result.author}</strong></a>
            <small class="trans_time" data-time="${result.ct}"></small>
            <div class="md-content">${md.render(result.content)}</div>
            <hr>
        </div>`
    e('#comments-body').insertAdjacentHTML('beforeEnd', contentHtml)
    e('#num_of_comments').innerText = Number(e('#num_of_comments').innerText) + 1

}
var clickAddComment = function(form) {
        apiAddComment(form, function(r) {
            var result = JSON.parse(r)
            insert_new_comment(result)
        })
}
var bindEventAddComment = function () {
    if ((e('#newComment') === null) || (e('#newComment').value = '')){
        return false
    }
    else{
        var btn = e('#btn_comment_submit')
        btn.addEventListener('click', function() {
            var form = {
            'topic_id': parseInt(e('#topic_id').value),
            'content': e('#newComment').value
            }
            clickAddComment(form)
            e('#newComment').value = ''
        })
    }
}


// 绑定的事件
var bindEvents = function() {
    bindEventAddComment()
}
// 主函数
var main = function () {
    markdownArticle()
    transTime()
    getCommentsOnLoad()
    bindEvents()
}
main()