/**
 * Created by Fanwu on 2017/7/6.
 */

// 获得该页的主题
var getTopics_by_page = function (allTopics, page) {
    var index0 = (page - 1) * 20
    return allTopics.slice(index0, index0+20)
}

// 插入分页
var insert_topic_pagination = function (num_of_topics) {
    var num_of_pages = Math.ceil(num_of_topics / 20)
    var pageination_frame = `
            <nav aria-label="Page navigation" class="container-fluid">
              <ul class="pagination">
              </ul>
            </nav>`
    e('#topics-table').insertAdjacentHTML('beforeEnd', pageination_frame)
    while(num_of_pages) {
        var pageination_content = `<li class="page" id="page${num_of_pages}"><a class="page_a">${num_of_pages}</apage_a></li>`
        e('.pagination').insertAdjacentHTML('afterBegin', pageination_content)
        num_of_pages--
    }
}

// 绑定分页按钮
var bindEventGetTopics_Page = function (allTopics) {
    var btns = e('.pagination')
    btns.addEventListener('click', function (event) {
        var btn = event.target
        var page = parseInt(btn.text)
        if(btn.classList.contains('page_a')) {
            insertAll(allTopics, page)
            $("html,body").animate({ scrollTop: 0}, 200)
        }
    })
}

// 插入主题
var insertTopics = function (allTopics, page) {
    var topicsTableFrame = `
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th class="col-md-7">主题</th>
                    <th>回复</th>
                    <th>点击</th>
                    <th>最近活动</th>
                </tr>
            </thead>
            <tbody id="tbody">
            </tbody>
        </table>`
    e('#topics-table').insertAdjacentHTML('afterBegin', topicsTableFrame)
    var topics = getTopics_by_page(allTopics, page)
    var i = topics.length
    while(i--) {
        var topic = topics[i]
        var htmlContent = `
            <tr>
                <th scope="row">
                    <a href="topics/${topic.id}">${topic.title}</a> <a class="tag" href="/bbs/?board=${topic.board_name}">${topic.board_name}</a>
                </th>
                <td>${topic.comments}</td>
                <td>${topic.hits}</td>
                <td class="trans_time" data-time="${topic.ut}"></td>
            </tr>
        `
        e('#tbody').insertAdjacentHTML('afterBegin', htmlContent)
    }
    if ($('.page').hasClass('disabled')) {
        $('.page').removeClass('disabled')
    }
    $(`#page${page}`).addClass('disabled')
}

// 插入主题和分页
var insertAll = function (allTopics, page) {
    e('#topics-table').innerHTML = ''
    insert_topic_pagination(allTopics.length)
    insertTopics(allTopics, page)
    bindEventGetTopics_Page(allTopics)
}


var on_load = function () {
    var board_name = getUrlVars()['board']
    var form = {
        'board_name': board_name,
    }
    apiTopics(form, function (r) {
        var allTopics = JSON.parse(r)
        insertAll(allTopics, 1)
    })
}

var main = function () {
    on_load()
    transTime()
}

main()
