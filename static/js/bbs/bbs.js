/**
 * Created by Fanwu on 2017/7/3.
 */
// API
var apiAddComment = function (form, callback) {
    var path = `/bbs/new_comment?token=${e('#crfs-token').value}`
    ajax('POST', path, form, callback)
}
var apiComments = function (form, callback) {
    var path = '/bbs/comments'
    ajax('POST', path, form, callback)
}
var apiTopics = function (form, callback) {
    var path = '/bbs/topics'
    ajax('POST', path, form, callback)
}
