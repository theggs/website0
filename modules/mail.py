from . import Module


class Mail(Module):
    __fields__ = Module.__fields__ + [
        ('title', str, ''),
        ('content', str, ''),
        ('from', int, -1),
        ('to', int, -1),
        ('read', bool, False),
    ]

    @classmethod
    def set_as_read(cls, mail_id):
        m = cls.find_one(id=mail_id)
        m.read = True
        m.save()
        return m
