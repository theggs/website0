from modules import Module
import markdown
md = markdown.Markdown(extensions=['markdown.extensions.toc', 'del_ins'])


class Article(Module):
    __fields__ = Module.__fields__ + [
        ('title', str, ''),
        ('subtitle', str, ''),
        ('author', str, 'Fanwu'),
        ('body', str, ''),
    ]

    @staticmethod
    def new_article(**kwargs):
        """
        通过参数建立新的 Article 实例, 不保存
        """
        title = kwargs.get('title', -1)
        if Article.find_one(title=title) is not None:
            print('Blog Exist!')
            return None
        else:
            article = Article.new(kwargs)
            article.save()
            return article

    @staticmethod
    def properties_fromfile(path):
        """
        分析文章内容返回title, subtitle, body
        """
        title, subtitle, index_body = '', '', 0
        with open(path, 'r', encoding='utf-8') as f:
            article = f.read()
            article_lines = article.splitlines()
            for n, i in enumerate(article_lines):
                if i[:2] == '# ':
                    title = i[2:]
                    index_body = n
                elif i[:3] == '## ':
                    subtitle = i[3:]
                    index_body = n
                    break
            body = '\n'.join(article_lines[index_body + 1:])
            body = md.convert(body)
        return dict(title=title, subtitle=subtitle, body=body)

    def update(self, form):
        """
        修改实例属性，保存
        """
        for key, value in form.items():
            if not hasattr(self, key):
                print(hasattr(self, key), key)
                print('{}: input error! '.format(key))
            else:
                setattr(self, key, value)
        self.save()
