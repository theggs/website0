from . import Module


class Comment(Module):
    __fields__ = Module.__fields__ + [
        ('content', str, ''),
        ('user_id', int, -1),
        ('user_username', str, ''),
        ('topic_id', int, -1),
    ]
