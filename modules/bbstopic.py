from . import Module


class Topic(Module):
    __fields__ = Module.__fields__ + [
        ('title', str, ''),
        ('content', str, ''),
        ('user_id', int, -1),
        ('board_name', str, ''),
        ('hits', int, 0),
        ('comments', int, 0),
    ]

    @classmethod
    def on_hit(cls, topic_id):
        topic = cls.find_one(id=topic_id)
        topic.hits += 1
        topic.save_noupdate_ut()
        return topic

    @classmethod
    def on_comment(cls, topic_id):
        topic = cls.find_one(id=topic_id)
        topic.comments += 1
        topic.save()
        return topic

    @classmethod
    def topic_brd_moving(cls, topic_id, board_name):
        from route import current_user
        from modules.bbscomment import Comment
        user = current_user()
        t = cls.find_one(id=int(topic_id))
        t.board_name = board_name
        t.save()
        form = dict(
            topic_id=topic_id,
            user_id=user.id,
            user_username=user.username,
            content='管理员将这个主题移动到了 **{}** 面板'.format(board_name)
        )
        nc = Comment.new(form)
        cls.on_comment(topic_id=int(topic_id))
        nc.save()
