# from utils import log
import time
from config import dbname
from pymongo import MongoClient


client = MongoClient()
db = client[dbname]


def nextid(name):
    from pymongo import ReturnDocument
    query = {
        'name': name
    }
    update = {
        '$inc': {
            'seq': 1
        }
    }
    kwargs = dict(
        upsert=True,
        return_document=ReturnDocument.AFTER
    )
    doc = db['data_id']
    new_id = doc.find_one_and_update(query, update, **kwargs).get('seq')
    return new_id


'''
class Module(object):
    def __init__(self, form):
        self.ct = form.get('ct', time.time())
        self.et = form.get('et', self.ct)
        self.id = form.get('id', None)
'''


class Module(object):
    __fields__ = [
        ('id', int, -1),
        ('deleted', bool, False),
        ('ct', float, 0),
        ('ut', float, 0),
    ]

    def collection_name(self):
        return self.__class__.__name__

    @classmethod
    def new(cls, form, **kwargs):
        m = cls()
        fields = cls.__fields__.copy()
        for f in fields:
            k, t, v = f
            if k in form:
                setattr(m, k, t(form[k]))
            else:
                setattr(m, k, v)
        for k, v in kwargs.items():
            if hasattr(m, k):
                setattr(m, k, v)
            else:
                raise KeyError
        m.id = nextid(cls.__name__)
        m.ct = time.time()
        m.ut = m.ct
        return m

    def delete(self):
        _filter = {
            'id': getattr(self, 'id'),
        }
        update = {
            '$set': {
                'deleted': True
            }
        }
        db[self.collection_name()].update_one(_filter, update)

    def update_one(self, form, **kwargs):
        _filter = form
        update = {'$set': kwargs}
        db[self.collection_name()].update_one(_filter, update)

    def save(self):
        setattr(self, 'ut', time.time())
        db[self.collection_name()].save(self.__dict__)

    def save_noupdate_ut(self):
        db[self.collection_name()].save(self.__dict__)

    @classmethod
    def _instance_from_bson(cls, bson):
        """
        内部使用，从数据库返回的数据建立实例
        :param bson: 数据库返回的数据
        :return: 建立的实例
        """
        m = cls()
        for k in bson:
            setattr(m, k, bson[k])
        return m

    @classmethod
    def find(cls, **kwargs):
        result = db[cls.__name__].find(kwargs)
        if result.count() == 0:
            return []
        else:
            return [cls._instance_from_bson(i) for i in result if i.get('deleted') is False]

    @classmethod
    def find_and_sort_by(cls, key_or_list, direction=None, **kwargs):
        result = db[cls.__name__].find(kwargs).sort(key_or_list, direction)
        if result.count() == 0:
            return []
        else:
            return [cls._instance_from_bson(i) for i in result if i.get('deleted') is False]

    @classmethod
    def find_one(cls, **kwargs):
        result = db[cls.__name__].find_one(kwargs)
        if result is None:
            return None
        else:
            return cls._instance_from_bson(result)

    @classmethod
    def all(cls):
        result = db[cls.__name__].find()
        return [cls._instance_from_bson(i) for i in result if i.get('deleted') is False]

    def data(self):
        dct = self.__dict__
        dct.pop('_id')
        return dct

    def __repr__(self):
        dct = self.__dict__
        lst = ['{}: {}'.format(k, dct[k]) for k in dct]
        return '< {}\n{} >\n'.format(self.__class__.__name__, '\n'.join(lst))
