from . import Module


class Board(Module):
    __fields__ = Module.__fields__ + [
        ('name', str, ''),
    ]
