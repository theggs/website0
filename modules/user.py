from modules import Module
from config import password_salt


class User(Module):
    __fields__ = Module.__fields__ + [
        ('username', str, ''),
        ('password', str, ''),
        ('isadmin', bool, False),
        ('avator', str, '/uploads/avator/default.jpg')
    ]

    @staticmethod
    def saulted_password(password, salt=password_salt):
        import hashlib

        def hash256(pwd):
            return hashlib.sha256(pwd.encode('ascii')).hexdigest()

        hash1 = hash256(password)
        hash2 = hash256(hash1 + salt)
        return hash2

    @classmethod
    def register(cls, form):
        username = form.get('username', '')
        password = form.get('password', '')
        hashed_password = cls.saulted_password(password)
        legality = 'abcdefghijklmnopqrstuvwxyz1234567890_'

        def if_in_legality(string):
            for s in string:
                if s not in legality:
                    return False
            return True
        legal = [User.find_one(username=username) is None,
                 if_in_legality(username),
                 len(username) >= 3,
                 len(username) <= 10,
                 if_in_legality(password),
                 len(password) >= 5,
                 len(password) <= 15,
                 ]
        print(legal)
        if False not in legal:
            user = cls.new(form)
            user.password = hashed_password
            user.save()
            return user
        else:
            return None

    @classmethod
    def login(cls, form):
        username = form.get('username', '')
        password = form.get('password', '')
        salted_password = cls.saulted_password(password)
        user = cls.find_one(username=username)
        if user is not None and user.password == salted_password:
            return user
        else:
            return None
