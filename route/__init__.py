from flask import (
    redirect,
    request,
    abort,
    session,
)

from modules.user import User


def current_user():
    user_id = session.get('user_id', -1)
    user = User.find_one(id=user_id)
    return user


def redirect_from_query(default_page='/'):
    redirect_page = request.args.get('redirect')
    if redirect_page is None:
        return redirect(default_page)
    else:
        return redirect(redirect_page)


csrf_tokens = {}


def csrf_token():
    import uuid
    token = str(uuid.uuid4())
    csrf_tokens[token] = getattr(current_user(), 'id', -1)
    return token


def csrf_valid():
    token = request.args.get('token')
    if token in csrf_tokens and csrf_tokens[token] == current_user().id:
        csrf_tokens.pop(token)
        return True
    else:
        return False
