from flask import (
    Blueprint,
    render_template,
    request,
    url_for,
    redirect,
    jsonify,
    abort,
    )

from modules.bbstopic import Topic
from modules.bbscomment import Comment
from modules.user import User
from modules.bbsboard import Board
from modules.mail import Mail
from route import current_user
from . import (
    csrf_valid,
    csrf_token,
)


main = Blueprint('bbs', __name__)


@main.route('/functions')
def bbs_functions():
    with open('db/bbs_func.md', 'r', encoding='utf-8') as f:
        bbs_func_md = f.read()
    return render_template('bbs/BBS_function.html', bbs_func_md=bbs_func_md)


# topic
def func_add_topic(form, **kwargs):
    topic = Topic.new(form, **kwargs)
    topic.save()
    return topic


def func_topics_by_board():
    board_name = request.json.get('board_name')
    if board_name is None or board_name == 'all':
        ts = Topic.find_and_sort_by('ut', -1)
    else:
        ts = Topic.find_and_sort_by('ut', -1, board_name=board_name)
    if ts is None:
        return None
    else:
        topics_of_board = [t.data() for t in ts]
        return topics_of_board


@main.route('/')
def index():
    boards = Board.all()
    return render_template('bbs/index.html',
                           boards=boards,
                           )


@main.route('/topics', methods=['POST'])
def topics():
    return jsonify(func_topics_by_board())


@main.route('/new_topic', methods=['GET', 'POST'])
def new_topic():
    user = current_user()
    if user is None:
        return redirect(url_for('user.login', redirect=request.path))

    if request.method == 'GET':
        return render_template('bbs/new_topic.html', boards=Board.all(), token=csrf_token())
    else:
        if csrf_valid():
            topic = func_add_topic(request.form, user_id=user.id)
            return redirect(url_for('.topic_page', topic_id=topic.id))
        else:
            return abort(403)


@main.route('/del_topic/<int:topic_id>', methods=['POST'])
def del_topic(topic_id):
    if current_user().isadmin is True and csrf_valid() is True:
        Topic.find_one(id=topic_id).delete()
        return redirect(url_for('.index'))
    else:
        return abort(403)


@main.route('/topics/<int:topic_id>')
def topic_page(topic_id):
    topic = Topic.on_hit(topic_id=topic_id)
    token = csrf_token()
    return render_template('bbs/topic.html',
                           topic=topic,
                           author=User.find_one(id=topic.user_id),
                           user=current_user(),
                           comments=Comment.find(topic_id=topic_id),
                           boards=Board.all(),
                           token=token,
                           )


# comment
def func_add_comment(form, **kwargs):
    comment = Comment.new(form, **kwargs)
    comment.save()
    Topic.on_comment(topic_id=form['topic_id'])
    return comment


def func_comments_of_topic_by_page():
    # 一页显示10个评论
    topic_id = request.json.get('topic_id', -1)
    comments_page = request.json.get('comments_page', 1)
    comments_of_topic = Comment.find(topic_id=topic_id)
    if comments_of_topic is None:
        return None
    else:
        comments_of_topic = [
            dict(ct=c.ct,
                 user_username=c.user_username,
                 user_id=c.user_id,
                 content=c.content,
                 )
            for c in Comment.find(topic_id=topic_id)
        ]
        index0 = (comments_page - 1) * 10
        return comments_of_topic[index0:index0+10]


@main.route('/new_comment', methods=['POST'])
def new_comment():
    currentuser = current_user()
    if currentuser is None:
        return redirect(url_for('user.lgoin', redirect=request.path))
    else:
        if csrf_valid():
            comment = func_add_comment(request.json,
                                       user_id=currentuser.id,
                                       user_username=currentuser.username,
                                       )
            return jsonify(
                    {
                        'ct': comment.ct,
                        'content': comment.content,
                        'author': currentuser.username,
                        'author_id': currentuser.id,
                    }
                )
        else:
            return abort(404)


@main.route('/comments', methods=['POST'])
def comments():
    return jsonify(func_comments_of_topic_by_page())


# 板块
def func_new_board():
    if current_user() is not None and current_user().isadmin is True:
        nb = Board.new(request.form)
        nb.save()
        return redirect(url_for('user.index'))
    else:
        return abort(403)


def func_topic_brd_moving():
    if current_user().isadmin is True:
        Topic.topic_brd_moving(topic_id=request.form['topic_id'], board_name=request.form['board_name'])
        return redirect(url_for('.topic_page', topic_id=request.form['topic_id']))
    else:
        return abort(403)


@main.route('/new_board', methods=['POST'])
def new_board():
    if csrf_valid():
        return func_new_board()
    else:
        return abort(403)


@main.route('/tpc_brd_mv', methods=['POST'])
def topic_brd_moving():
    if csrf_valid():
        return func_topic_brd_moving()
    else:
        return abort(403)
