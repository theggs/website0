from flask import (
    render_template,
    Blueprint,
    request,
    redirect,
    session,
    url_for,
    abort,
    send_from_directory,
)

from modules.user import User
from modules.mail import Mail
from modules.bbstopic import Topic
from . import (
    redirect_from_query,
    csrf_token,
    csrf_valid,
    current_user,
)


main = Blueprint('user', __name__)


def valid_login():
    user = User.login(request.form)
    if user is None:
        return redirect(url_for('.login'))
    else:
        session['user_id'] = user.id
        return redirect_from_query(url_for('.index'))


def valid_register(form):
    user = User.register(form)
    if user is None:
        return redirect(url_for('.register'))
    else:
        session['user_id'] = user.id
        return redirect_from_query(url_for('.index'))


@main.route('/')
def index():
    user = current_user()
    if current_user() is None:
        return redirect(url_for('.login', redirect=url_for('.index')))
    return render_template('user/index.html',
                           user=user,
                           )


@main.route('/profile')
def profile():
    user = current_user()
    mails_unread, recent_topics, token = [], [], ''
    if user is not None:
        mails_unread = Mail.find(to=user.id, read=False)
        recent_topics = Topic.find_and_sort_by('ct', -1, user_id=user.id)[:10]
        token = csrf_token()
    return render_template('user/profile.html',
                           user=user,
                           mails_unread=mails_unread,
                           recent_topics=recent_topics,
                           token=token,
                           )


@main.route('/<int:user_id>')
def user_page(user_id):
    if current_user() is None:
        return redirect(url_for('.login', redirect=url_for('.user_page', user_id=user_id)))

    token = csrf_token()
    recent_topics = Topic.find_and_sort_by('ct', -1, user_id=user_id)[:10]
    return render_template('user/user.html',
                           this_user=User.find_one(id=user_id),
                           token=token,
                           recent_topics=recent_topics,
                           )


@main.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('user/login.html')
    else:
        return valid_login()


@main.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'GET':
        return render_template('user/register.html')
    else:
        return valid_register(request.form)


@main.route('/logout')
def logout():
    if current_user() is not None:
        session.pop('user_id')
    return redirect_from_query(url_for('.login'))


# Mail
def func_send_mail():
    form = {
        'title': request.form.get('title', ''),
        'content': request.form.get('content', ''),
        'to': int(request.form.get('to', -1)),
        'from': getattr(current_user(), 'id'),
    }
    m = Mail.new(form)
    m.save()
    return m


@main.route('/send_mail', methods=['POST'])
def send_mail():
    if current_user() is None:
        return redirect(url_for('.login', redirect=request.path))

    if csrf_valid() is True:
        func_send_mail()
        return redirect_from_query()
    # todo: 改为 Ajax
    else:
        abort(403)


@main.route('/mail/<int:mail_id>')
def read_mail(mail_id):
    mail = Mail.set_as_read(mail_id)
    if current_user() is None or mail.to != current_user().id:
        return redirect(url_for('.login', redirect=request.path))
    user_from = User.find_one(id=getattr(mail, 'from'))
    return render_template('user/read_mail.html', mail=mail, user_from=user_from)


# avator
def deal_with_avator_file(file):
    from PIL import Image
    from imghdr import what
    import os
    user = current_user()

    filetype = what(file)
    folder = 'uploads'
    folder2 = 'avator'
    filename = str(user.id) + str(filetype)
    path = os.path.join(folder, folder2, filename)

    size = 128, 128

    with Image.open(file) as avator:
        avator.thumbnail(size)
        avator.save(path, str(filetype))

    user.avator = '/' + str(path)
    user.save()


@main.route('/upload_avator', methods=['POST'])
def upload_avator():
    if csrf_valid():
        deal_with_avator_file(request.files['avator'])
        return redirect_from_query(url_for('.index'))
    else:
        abort(403)

