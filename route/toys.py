from flask import (
    render_template,
    Blueprint,
    request,
    jsonify,
    redirect,
    )

from config import domain
from utils import random_letters


main = Blueprint('toys', __name__)


# ----短链接----

url_ref = {}  # 短链接库


def deal_url_post(form):
    """
    处理短链接的 添加/查询 请求
    :param form: 传入数组。添加请求格式为{'new': data}。查询请求格式为{'lookup': data}
    :return: JSON response，内容是 url。
    """
    k, v = '', ''
    for key, value in form.items():
        k, v = key, value
    if k == 'new':
        if v.find("http://") != 0 and v.find("https://") != 0:
            v = 'http://' + v
        shortkey = random_letters(6)
        url_ref[shortkey] = v
        return jsonify(domain + '/toys/' + shortkey)
    elif k == 'lookup':
        v = v.split('/')[-1]
        return jsonify(url_ref[v])


@main.route('/shorturl', methods=['GET', 'POST'])
def shorturl():
    """
    短链接页面
    """
    if request.method == 'GET':
        return render_template('toys/shorturl.html')
    elif request.method == 'POST':
        return deal_url_post(request.json)


@main.route('/<url_shorted>')
def shorturl_redirect(url_shorted):
    """
    短链接跳转页面
    :param url_shorted:
    :return:
    """
    return redirect(url_ref.get(url_shorted, request.host_url))
# ----短链接完了----


# ----聊天室----

@main.route('/chatroom')
def chatroom():
    return render_template('toys/chatroom.html')

# ----聊天室完了----


@main.route('/')
def index():
    return render_template('toys/index.html')
