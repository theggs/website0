from flask import (
    Blueprint,
    redirect,
    url_for,
    )

main = Blueprint('index', __name__)


@main.route('/')
def index():
    return redirect(url_for('blog.index'))
