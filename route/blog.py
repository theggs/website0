from flask import (
    render_template,
    Blueprint,
)

from modules.article import Article

main = Blueprint('blog', __name__)


@main.route('/<int:article_id>')
def post(article_id):
    article = Article.find_one(id=article_id)
    return render_template('blog/article.html', article=article)


@main.route('/')
def index():
    articles = Article.all()
    return render_template('blog/index.html', articles=articles)
