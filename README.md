# README #

这是我的个人网站的源码，包括运行在上面的几个项目。

This is the codes of my website, which include several projects.

### Projects ###

* Blog
* Shorturl
* BBS

### How do I get set up? ###

需要在项目根目录建立一个 config.py 。里面包括如下内容：

An extra python file named 'config.py' is need. Codes:

    secret_key = ''         # Flask's secert_key
    dbname = ''             # 给数据库起个名 database's name
    domain = ''             # 域名 domain
    password_salt = ''      # 密码加密用的盐 the salt which is used to encryption passwords
    
部署所需的文件在 /setup 里。请删掉 website0.nginx 里的 ``rewrite ^(.*) https://$host$1 permanent;``
