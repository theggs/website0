from modules.article import Article


def crrct(article):
    """
    更正 blog 实例的属性
    """
    while True:
        response = input('\ntitle: {}\nsubtitle: {}\nauthor: {}\n\nRight? (any key or "n") '
                         .format(article.title, article.subtitle, article.author, article.author))
        if response == 'n':
            list_after_crrct = []
            input_correct = input('\nInput what to recorrect like this:\n   title: IamTitle, author: Peggy\n')
            list_input = input_correct.split(', ')
            for i in list_input:
                list_after_crrct.append(i.split(': '))
            form = dict(list_after_crrct)
            article.update(form)
        else:
            break


def entry(path):
    properties = Article.properties_fromfile(path)
    article = Article.new_article(**properties)
    if article is not None:
        crrct(article)


def update(path):
    properties = Article.properties_fromfile(path)
    article = Article.find_one(title=properties.get('title', ''))
    if article is None:
        print('Article is not exist. Please create one first!')
        main()
    else:
        article.update(properties)


def main():
    folder = 'db/article/'
    filename = input('Enter the filename: ')
    path = folder + filename
    if filename[-3:] != '.md':
        print('Incorrect filename!\n')
        main()
    else:
        command = input('1. Create a new article.\n2. Update an article.\n')
        if command == '1':
            entry(path)
        elif command == '2':
            update(path)
        else:
            main()


if __name__ == '__main__':
    main()
