apt-get install git python3 python3-pip python3-setuptools supervisor mongodb redis-server zsh nginx

pip3 install flask
pip3 install markdown
pip3 install pillow
pip3 install gunicorn
pip3 install pymongo
pip3 install mdx-del-ins
