bind = '127.0.0.1:2001'
pid = '/tmp/website0.pid'
access_log_format = '%(h)s %(l)s %(u)s "%(r)s" %(s)s %(b)s "%(f)s" "%(a)s"'
accesslog = "/root/website0/logs/gunicornacess"
errorlog = "/root/website0/logs/gunicornerror"
